# 🚀 Desafio Front end Platos

Bem-vindo(a)! Este é o desafio Front end da Platos!

O objetivo deste desafio é avaliar suas habilidades de programação.
Quando sua solução estiver pronta, basta responder o e-mail que recebeu com o link do seu repo aqui no GitLab ou GitHub!

Caso tenha alguma dúvida, basta responder o e-mail que recebeu com sua dúvida e será um prazer te ajudar!
Bom desafio!

> ⚠️ **É importante que o seu repo esteja público, caso contrário não iremos conseguir avaliar sua resposta**

---
## ![contexto](contexto.png)  Contexto

O desafio será implementar uma tela de gerenciamento de cursos com as seguintes funcionalidades:
-  Listagem de cursos com paginação
-  Buscar por nome do curso ou grau
-  Limpar campo de busca, ao realizar essa ação não esquecer de exibir todos os cursos novamente
-  Criar novo curso: 
    - Nesta tela os selects *Nível escolar e Grau* que são encadeados. **Ex: Ao selecionar o Nível Escolar Graduação o select Grau deverá carregar os graus do nível selecionado**.
    - Ao criar um novo curso não permitir que seja cadastrado curso com o mesmo nome e grau.
-  Editar dados do curso
-  Validação dos dados obrigatórios nas ações de *Criar e Editar curso*
-  Remover curso

---
## 📋 Instruções

Vamos colocar a mão na massa?

- Siga [esse protótipo](https://www.figma.com/file/rMUfJKPJPAqQOxThzBeSHe/Challenge-Front-end-Platos?node-id=0%3A1)
- Utilize Next.js e Typescript
- Faça a estilização utilizando a biblioteca de componentes [Chakra UI](https://chakra-ui.com/getting-started)
- Por favor, inclua no README as instruções de instalação do projeto
- Sinta-se livre para incluir quaisquer observações
- Para consumir os dados e criar uma API Fake você pode utilizar a ferramenta [JSON Server](https://github.com/typicode/json-server#getting-started). Basta realizar a instalação global da ferramenta em sua máquina e incluir o arquivo ***db.json*** que criamos para você na raiz do seu projeto. **OBS:.** Recomendamos o seguinte conteúdo com informações sobre a utilização do JSON Server : [Simulando uma API REST com JSON Server de maneira simples](https://www.fabricadecodigo.com/json-server/)
---
## &check; Critérios de Avaliação

Além dos requisitos levantados acima, iremos avaliar para os seguintes critérios durante a análise do desafio entregue:

- Gerenciamento de estado
- Componentização
- Responsividade
- Preocupação com usabilidade
- Padrões de código
- Padrão de commits (_Conventional_)
---
## 😎 Seria legal
- Preocupação com acessibilidade
- Testes unitários
- Utilização de cache do Next.js

---

_O desafio acima foi cuidadosamente construído para propósitos de avaliação apenas._
